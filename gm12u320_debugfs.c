/*
 * Copyright (c) 2017 Marco Diego Aurélio Mesquita <marcodiegomesquita@gmail.com>
 *
 * Parts of this file were based on sources as follows:
 *
 *  Copyright © 2017 Broadcom
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/seq_file.h>
#include <drm/drmP.h>
#include "gm12u320_drm.h"

#define REGDEF(reg) { reg, #reg }
static const struct {
	u32 reg;
	const char *name;
} gm12u320_reg_defs[] = {
	REGDEF(0),
	REGDEF(1),
	REGDEF(2),
	REGDEF(3),
	REGDEF(4),
	REGDEF(5),
	REGDEF(6),
};

int gm12u320_debugfs_regs(struct seq_file *m, void *unused)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(gm12u320_reg_defs); i++) {
		seq_printf(m, "%s (0x%04x): 0x%08x\n",
			   gm12u320_reg_defs[i].name, gm12u320_reg_defs[i].reg,
			   0);
	}

	return 0;
}

static const struct drm_info_list gm12u320_debugfs_list[] = {
	{"regs", gm12u320_debugfs_regs, 0},
};

int
gm12u320_debugfs_init(struct drm_minor *minor)
{
	return drm_debugfs_create_files(gm12u320_debugfs_list,
					ARRAY_SIZE(gm12u320_debugfs_list),
					minor->debugfs_root, minor);
}
