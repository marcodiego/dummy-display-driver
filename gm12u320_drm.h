/*
 * Copyright (c) 2017 Marco Diego Aurélio Mesquita <marcodiegomesquita@gmail.com>
 *
 * Parts of this file were based on sources as follows:
 *
 * (C) COPYRIGHT 2012-2013 ARM Limited. All rights reserved.
 * Copyright (c) 2006-2008 Intel Corporation
 * Copyright (c) 2007 Dave Airlie <airlied@linux.ie>
 * Copyright (C) 2011 Texas Instruments
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms of
 * such GNU licence.
 *
 */

#ifndef _GM12U320_DRM_H_
#define _GM12U320_DRM_H_

#include <linux/usb.h>
#include <linux/spinlock.h>
#include <drm/drm_gem.h>
#include <drm/drm_simple_kms_helper.h>
#include <linux/clk-provider.h>

#define DRIVER_NAME		"gm12u320"
#define DRIVER_DESC		"Grain Media GM12U320 USB projector display"
#define DRIVER_DATE		"20150107"

#define DRIVER_MAJOR		0
#define DRIVER_MINOR		0
#define DRIVER_PATCHLEVEL	1

#define GM12U320_USER_WIDTH	852
#define GM12U320_REAL_WIDTH	854
#define GM12U320_HEIGHT		480

#define GM12U320_BLOCK_COUNT	20


struct drm_minor;

struct gm12u320_drm_connector {
	struct drm_connector connector;
	struct drm_panel *panel;
};

struct gm12u320_drm_dev_private {
	struct drm_device *drm;

	struct gm12u320_drm_connector connector;
	struct drm_simple_display_pipe pipe;
	struct drm_fbdev_cma *fbdev;
	struct usb_device *udev;

	void *regs;
	/* The pixel clock (a reference to our clock divider off of CLCDCLK). */
	struct clk *clk;
	/* gm12u320's internal clock divider. */
	struct clk_hw clk_div;
	/* Lock to sync access to CLCD_TIM2 between the common clock
	 * subsystem and gm12u320_display_enable().
	 */
	spinlock_t tim2_lock;

	unsigned char *cmd_buf;
	unsigned char *data_buf[GM12U320_BLOCK_COUNT];
	struct {
		bool run;
		struct workqueue_struct *workq;
		struct work_struct work;
		wait_queue_head_t waitq;
		struct mutex lock;
		//struct gm12u320_framebuffer *fb;
		int x1;
		int x2;
		int y1;
		int y2;
	} fb_update;
};

#define to_gm12u320_connector(x) \
	container_of(x, struct gm12u320_drm_connector, connector)

int gm12u320_display_init(struct drm_device *dev);
int gm12u320_enable_vblank(struct drm_device *drm, unsigned int crtc);
void gm12u320_disable_vblank(struct drm_device *drm, unsigned int crtc);
int gm12u320_connector_init(struct drm_device *dev);
int gm12u320_encoder_init(struct drm_device *dev);
int gm12u320_dumb_create(struct drm_file *file_priv,
		      struct drm_device *dev,
		      struct drm_mode_create_dumb *args);
int gm12u320_debugfs_init(struct drm_minor *minor);

#endif /* _GM12U320_DRM_H_ */
