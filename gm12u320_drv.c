/*
 * Copyright (c) 2017 Marco Diego Aurélio Mesquita <marcodiegomesquita@gmail.com>
 *
 * Parts of this file were based on sources as follows:
 *
 * (C) COPYRIGHT 2012-2013 ARM Limited. All rights reserved.
 * Copyright (c) 2006-2008 Intel Corporation
 * Copyright (c) 2007 Dave Airlie <airlied@linux.ie>
 * Copyright (C) 2011 Texas Instruments
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms of
 * such GNU licence.
 *
 */

/**
 * DOC: TODO
 */

#include <linux/version.h>
#include <linux/shmem_fs.h>
#include <linux/dma-buf.h>
#include <linux/module.h>
#include <linux/slab.h>

#include <drm/drmP.h>
#include <drm/drm_atomic_helper.h>
#include <drm/drm_crtc_helper.h>
#include <drm/drm_gem_cma_helper.h>
#include <drm/drm_fb_cma_helper.h>

#include "gm12u320_drm.h"

#define MISC_RCV_EPT			1
#define DATA_RCV_EPT			2
#define DATA_SND_EPT			3
#define MISC_SND_EPT			4

#define DATA_BLOCK_HEADER_SIZE		84
#define DATA_BLOCK_CONTENT_SIZE		64512
#define DATA_BLOCK_FOOTER_SIZE		20
#define DATA_BLOCK_SIZE			(DATA_BLOCK_HEADER_SIZE + \
					 DATA_BLOCK_CONTENT_SIZE + \
					 DATA_BLOCK_FOOTER_SIZE)

#define DATA_LAST_BLOCK_CONTENT_SIZE	4032
#define DATA_LAST_BLOCK_SIZE		(DATA_BLOCK_HEADER_SIZE + \
					 DATA_LAST_BLOCK_CONTENT_SIZE + \
					 DATA_BLOCK_FOOTER_SIZE)

#define CMD_SIZE			31
#define READ_STATUS_SIZE		13
#define MISC_VALUE_SIZE			4

#define CMD_TIMEOUT			200
#define DATA_TIMEOUT			1000
#define IDLE_TIMEOUT			2000
#define FIRST_FRAME_TIMEOUT		2000

static const char cmd_data[CMD_SIZE] = {
	0x55, 0x53, 0x42, 0x43, 0x00, 0x00, 0x00, 0x00,
	0x68, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x10, 0xff,
	0x00, 0x00, 0x00, 0x00, 0xfc, 0x00, 0x80, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

static const char cmd_draw[CMD_SIZE] = {
	0x55, 0x53, 0x42, 0x43, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0xfe,
	0x00, 0x00, 0x00, 0xc0, 0xd1, 0x05, 0x00, 0x40,
	0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00
};

static const char cmd_misc[CMD_SIZE] = {
	0x55, 0x53, 0x42, 0x43, 0x00, 0x00, 0x00, 0x00,
	0x04, 0x00, 0x00, 0x00, 0x80, 0x01, 0x10, 0xfd,
	0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

static const char data_block_header[DATA_BLOCK_HEADER_SIZE] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0xfb, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x04, 0x15, 0x00, 0x00, 0xfc, 0x00, 0x00,
	0x01, 0x00, 0x00, 0xdb
};

static const char data_last_block_header[DATA_BLOCK_HEADER_SIZE] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0xfb, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x2a, 0x00, 0x20, 0x00, 0xc0, 0x0f, 0x00, 0x00,
	0x01, 0x00, 0x00, 0xd7
};

static const char data_block_footer[DATA_BLOCK_FOOTER_SIZE] = {
	0xfb, 0x14, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x80, 0x00, 0x00, 0x4f
};

static int gm12u320_usb_alloc(struct gm12u320_drm_dev_private *gm12u320)
{
	int i, block_size;
	const char *hdr;

	gm12u320->cmd_buf = kmalloc(CMD_SIZE, GFP_KERNEL);
	if (!gm12u320->cmd_buf)
		return -ENOMEM;

	for (i = 0; i < GM12U320_BLOCK_COUNT; i++) {
		if (i == GM12U320_BLOCK_COUNT - 1) {
			block_size = DATA_LAST_BLOCK_SIZE;
			hdr = data_last_block_header;
		} else {
			block_size = DATA_BLOCK_SIZE;
			hdr = data_block_header;
		}

		gm12u320->data_buf[i] = kzalloc(block_size, GFP_KERNEL);
		if (!gm12u320->data_buf[i])
			return -ENOMEM;

		memcpy(gm12u320->data_buf[i], hdr, DATA_BLOCK_HEADER_SIZE);
		memcpy(gm12u320->data_buf[i] +
				(block_size - DATA_BLOCK_FOOTER_SIZE),
		       data_block_footer, DATA_BLOCK_FOOTER_SIZE);
	}

	return 0;
}

/*static void gm12u320_usb_free(struct gm12u320_device *gm12u320)
{
	int i;

	for (i = 0; i < GM12U320_BLOCK_COUNT; i++)
		kfree(gm12u320->data_buf[i]);

	kfree(gm12u320->cmd_buf);
}*/

static int gm12u320_fb_update_ready(struct gm12u320_drm_dev_private *gm12u320)
{
	int ret;

	mutex_lock(&gm12u320->fb_update.lock);
	ret = !gm12u320->fb_update.run /*|| gm12u320->fb_update.fb != NULL*/;
	mutex_unlock(&gm12u320->fb_update.lock);

	return ret;
}

void gm12u320_start_fb_update(struct drm_device *dev)
{
	struct gm12u320_drm_dev_private *gm12u320 = dev->dev_private;

	printk("gm12u320_start_fb_update\n");
	mutex_lock(&gm12u320->fb_update.lock);
	gm12u320->fb_update.run = true;
	mutex_unlock(&gm12u320->fb_update.lock);

	queue_work(gm12u320->fb_update.workq, &gm12u320->fb_update.work);
}


static void gm12u320_fb_update_work(struct work_struct *work)
{
	struct gm12u320_drm_dev_private *gm12u320 =
		container_of(work, struct gm12u320_drm_dev_private,
				   fb_update.work);
	int draw_status_timeout = FIRST_FRAME_TIMEOUT;
	int block, block_size, len, x1, x2, y1, y2;
	//struct gm12u320_framebuffer *fb;
	int frame = 0;
	int ret = 0;

	printk("gm12u320_fb_update_work\n");

	while (gm12u320->fb_update.run) {
		mutex_lock(&gm12u320->fb_update.lock);
		//fb = gm12u320->fb_update.fb;
		x1 = gm12u320->fb_update.x1;
		x2 = gm12u320->fb_update.x2;
		y1 = gm12u320->fb_update.y1;
		y2 = gm12u320->fb_update.y2;
		//gm12u320->fb_update.fb = NULL;
		mutex_unlock(&gm12u320->fb_update.lock);

		/*if (fb) {
			gm12u320_copy_fb_to_blocks(fb, x1, x2, y1, y2);
			drm_framebuffer_unreference(&fb->base);
		}*/

		for (block = 0; block < GM12U320_BLOCK_COUNT; block++) {
			if (block == GM12U320_BLOCK_COUNT - 1)
				block_size = DATA_LAST_BLOCK_SIZE;
			else
				block_size = DATA_BLOCK_SIZE;

			/* Send data command to device */
			memcpy(gm12u320->cmd_buf, cmd_data, CMD_SIZE);
			gm12u320->cmd_buf[8] = block_size & 0xff;
			gm12u320->cmd_buf[9] = block_size >> 8;
			gm12u320->cmd_buf[20] = 0xfc - block * 4;
			gm12u320->cmd_buf[21] = block | (frame << 7);

			ret = usb_bulk_msg(gm12u320->udev,
				usb_sndbulkpipe(gm12u320->udev, DATA_SND_EPT),
				gm12u320->cmd_buf, CMD_SIZE, &len,
				CMD_TIMEOUT);
			if (ret || len != CMD_SIZE)
				goto err;

			/* Send data block to device */
			ret = usb_bulk_msg(gm12u320->udev,
				usb_sndbulkpipe(gm12u320->udev, DATA_SND_EPT),
				gm12u320->data_buf[block], block_size,
				&len, DATA_TIMEOUT);
			if (ret || len != block_size)
				goto err;

			/* Read status */
			ret = usb_bulk_msg(gm12u320->udev,
				usb_rcvbulkpipe(gm12u320->udev, DATA_RCV_EPT),
				gm12u320->cmd_buf, READ_STATUS_SIZE, &len,
				CMD_TIMEOUT);
			if (ret || len != READ_STATUS_SIZE)
				goto err;
		}

		/* Send draw command to device */
		memcpy(gm12u320->cmd_buf, cmd_draw, CMD_SIZE);
		ret = usb_bulk_msg(gm12u320->udev,
			usb_sndbulkpipe(gm12u320->udev, DATA_SND_EPT),
			gm12u320->cmd_buf, CMD_SIZE, &len, CMD_TIMEOUT);
		if (ret || len != CMD_SIZE)
			goto err;

		/* Read status */
		ret = usb_bulk_msg(gm12u320->udev,
			usb_rcvbulkpipe(gm12u320->udev, DATA_RCV_EPT),
			gm12u320->cmd_buf, READ_STATUS_SIZE, &len,
			draw_status_timeout);
		if (ret || len != READ_STATUS_SIZE)
			goto err;

		draw_status_timeout = CMD_TIMEOUT;
		frame = !frame;

		/*
		 * We must draw a frame every 2s otherwise the projector
		 * switches back to showing its logo.
		 */
		wait_event_timeout(gm12u320->fb_update.waitq,
				   gm12u320_fb_update_ready(gm12u320),
				   msecs_to_jiffies(IDLE_TIMEOUT));
	}
	return;
err:
	/* Do not log errors caused by module unload or device unplug */
	if (ret != -ECONNRESET && ret != -ESHUTDOWN)
		dev_err(&gm12u320->udev->dev, "Frame update error: %d\n", ret);
}


static struct drm_mode_config_funcs mode_config_funcs = {
	.fb_create = drm_fb_cma_create,
	.atomic_check = drm_atomic_helper_check,
	.atomic_commit = drm_atomic_helper_commit,
};

static int gm12u320_modeset_init(struct drm_device *dev)
{
	struct drm_mode_config *mode_config;
	struct gm12u320_drm_dev_private *priv = dev->dev_private;
	int ret = 0;

	drm_mode_config_init(dev);
	mode_config = &dev->mode_config;
	mode_config->funcs = &mode_config_funcs;
	dev->mode_config.min_width = GM12U320_USER_WIDTH;
	dev->mode_config.min_height = GM12U320_HEIGHT;

	dev->mode_config.max_width = 2048;
	dev->mode_config.max_height = 2048;

	dev->mode_config.prefer_shadow = 0;
	dev->mode_config.preferred_depth = 24;

	ret = gm12u320_connector_init(dev);
	if (ret) {
		dev_err(dev->dev, "Failed to create gm12u320_drm_connector\n");
		goto out_config;
	}

	/* Don't actually attach if we didn't find a drm_panel
	 * attached to us.  This will allow a kernel to include both
	 * the fbdev gm12u320 driver and this one, and choose between
	 * them based on which subsystem has support for the panel.
	 */

	ret = gm12u320_display_init(dev);
	if (ret != 0) {
		dev_err(dev->dev, "Failed to init display\n");
		goto out_config;
	}

	ret = drm_vblank_init(dev, 1);
	if (ret != 0) {
		dev_err(dev->dev, "Failed to init vblank\n");
		goto out_config;
	}

	drm_mode_config_reset(dev);

	priv->fbdev = drm_fbdev_cma_init(dev, 32,
					 dev->mode_config.num_connector);

	drm_kms_helper_poll_init(dev);

	goto finish;

out_config:
	drm_mode_config_cleanup(dev);
finish:
	return ret;
}

static const struct file_operations drm_fops = {
	.owner              = THIS_MODULE,
	.open               = drm_open,
	.release            = drm_release,
	.unlocked_ioctl     = drm_ioctl,
	.compat_ioctl       = drm_compat_ioctl,
	.poll               = drm_poll,
	.read               = drm_read,
	.llseek             = no_llseek,
	.mmap               = drm_gem_cma_mmap,
};

static void gm12u320_lastclose(struct drm_device *dev)
{
	struct gm12u320_drm_dev_private *priv = dev->dev_private;

	drm_fbdev_cma_restore_mode(priv->fbdev);
}

static struct drm_driver gm12u320_drm_driver = {
	.driver_features =
		DRIVER_MODESET | DRIVER_GEM | DRIVER_PRIME | DRIVER_ATOMIC,
	.lastclose = gm12u320_lastclose,
	.ioctls = NULL,
	.fops = &drm_fops,
	.name = DRIVER_NAME,
	.desc = DRIVER_DESC,
	.date = DRIVER_DATE,
	.major = DRIVER_MAJOR,
	.minor = DRIVER_MINOR,
	.patchlevel = DRIVER_PATCHLEVEL,
	.dumb_create = drm_gem_cma_dumb_create,
	.dumb_destroy = drm_gem_dumb_destroy,
	.dumb_map_offset = drm_gem_cma_dumb_map_offset,
	.gem_free_object = drm_gem_cma_free_object,
	.gem_vm_ops = &drm_gem_cma_vm_ops,

	.enable_vblank = gm12u320_enable_vblank,
	.disable_vblank = gm12u320_disable_vblank,

	.prime_handle_to_fd = drm_gem_prime_handle_to_fd,
	.prime_fd_to_handle = drm_gem_prime_fd_to_handle,
	.gem_prime_import = drm_gem_prime_import,
	.gem_prime_import_sg_table = drm_gem_cma_prime_import_sg_table,
	.gem_prime_export = drm_gem_prime_export,
	.gem_prime_get_sg_table	= drm_gem_cma_prime_get_sg_table,

#if defined(CONFIG_DEBUG_FS)
	.debugfs_init = gm12u320_debugfs_init,
#endif
};

static int gm12u320_usb_probe(struct usb_interface *interface,
			      const struct usb_device_id *id)
{
	struct device *dev = &interface->dev;
	struct usb_device *udev = interface_to_usbdev(interface);
	struct gm12u320_drm_dev_private *priv;
	struct drm_device *drm;
	int ret;

	/*
	 * The gm12u320 presents itself to the system as 2 usb mass-storage
	 * interfaces, for the second one we proceed successully with binding,
	 * but otherwise ignore it.
	 */
	if (interface->cur_altsetting->desc.bInterfaceNumber != 0)
		return 0;

	priv = devm_kzalloc(dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	drm = drm_dev_alloc(&gm12u320_drm_driver, dev);
	if (IS_ERR(drm))
		return PTR_ERR(drm);

	priv->drm = drm;
	priv->udev = udev;
	drm->dev_private = priv;

	INIT_WORK(&priv->fb_update.work, gm12u320_fb_update_work);
	mutex_init(&priv->fb_update.lock);
	init_waitqueue_head(&priv->fb_update.waitq);

	priv->fb_update.workq = create_singlethread_workqueue(DRIVER_NAME);
	if (!priv->fb_update.workq) {
		ret = -ENOMEM;
		goto dev_unref;
	}

	ret = gm12u320_usb_alloc(priv);
	if (ret)
		goto err_wq;

	DRM_DEBUG("\n");
	ret = gm12u320_modeset_init(drm);
	if (ret != 0)
		goto dev_unref;

	ret = drm_dev_register(drm, 0);
	if (ret < 0)
		goto dev_unref;

	usb_set_intfdata(interface, dev);
	DRM_INFO("Initialized gm12u320 on minor %d\n", drm->primary->index);

	gm12u320_start_fb_update(drm);
	return 0;

err_wq:
	destroy_workqueue(priv->fb_update.workq);
dev_unref:
	drm_dev_unref(drm);
	return ret;

}

static void gm12u320_usb_disconnect(struct usb_interface *interface)
{
	struct drm_device *dev = usb_get_intfdata(interface);

	if (!dev)
		return;

	drm_kms_helper_poll_disable(dev);
	drm_unplug_dev(dev);
}

#ifdef CONFIG_PM

int gm12u320_suspend(struct usb_interface *interface, pm_message_t message)
{
	struct drm_device *dev = usb_get_intfdata(interface);

	if (!dev)
		return 0;

	return 0;
}

int gm12u320_resume(struct usb_interface *interface)
{
	struct drm_device *dev = usb_get_intfdata(interface);

	if (!dev)
		return 0;

	return 0;
}
#endif

static struct usb_device_id id_table[] = {
	{ USB_DEVICE(0x1de1, 0xc102) },
	{},
};
MODULE_DEVICE_TABLE(usb, id_table);

static struct usb_driver gm12u320_driver = {
	.name = "gm12u320",
	.probe = gm12u320_usb_probe,
	.disconnect = gm12u320_usb_disconnect,
	.id_table = id_table,
#ifdef CONFIG_PM
	.suspend = gm12u320_suspend,
	.resume = gm12u320_resume,
	.reset_resume = gm12u320_resume,
#endif
};

module_usb_driver(gm12u320_driver);
MODULE_AUTHOR("Marco Diego Aurélio Mesquita <marcodiegomesquita@gmail.com>");
MODULE_LICENSE("GPL");
