/*
 * Copyright (c) 2017 Marco Diego Aurélio Mesquita <marcodiegomesquita@gmail.com>
 *
 * Parts of this file were based on sources as follows:
 *
 * (C) COPYRIGHT 2012-2013 ARM Limited. All rights reserved.
 * Copyright (c) 2006-2008 Intel Corporation
 * Copyright (c) 2007 Dave Airlie <airlied@linux.ie>
 * Copyright (C) 2011 Texas Instruments
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms of
 * such GNU licence.
 *
 */

#include <linux/clk.h>
#include <linux/version.h>
#include <linux/dma-buf.h>
#include <linux/of_graph.h>

#include <drm/drmP.h>
#include <drm/drm_panel.h>
#include <drm/drm_gem_cma_helper.h>
#include <drm/drm_fb_cma_helper.h>

#include "gm12u320_drm.h"

static u32 gm12u320_get_fb_offset(struct drm_plane_state *pstate)
{
	struct drm_framebuffer *fb = pstate->fb;
	struct drm_gem_cma_object *obj = drm_fb_cma_get_gem_obj(fb, 0);

	return (obj->paddr +
		fb->offsets[0] +
		fb->format->cpp[0] * pstate->src_x +
		fb->pitches[0] * pstate->src_y);
}

static int gm12u320_display_check(struct drm_simple_display_pipe *pipe,
			       struct drm_plane_state *pstate,
			       struct drm_crtc_state *cstate)
{
	const struct drm_display_mode *mode = &cstate->mode;
	struct drm_framebuffer *old_fb = pipe->plane.state->fb;
	struct drm_framebuffer *fb = pstate->fb;

	if (mode->hdisplay % 16)
		return -EINVAL;

	if (fb) {
		u32 offset = gm12u320_get_fb_offset(pstate);

		/* FB base address must be dword aligned. */
		if (offset & 3)
			return -EINVAL;

		/* There's no pitch register -- the mode's hdisplay
		 * controls it.
		 */
		if (fb->pitches[0] != mode->hdisplay * fb->format->cpp[0])
			return -EINVAL;

		/* We can't change the FB format in a flicker-free
		 * manner (and only update it during CRTC enable).
		 */
		if (old_fb && old_fb->format != fb->format)
			cstate->mode_changed = true;
	}

	return 0;
}

static void gm12u320_display_enable(struct drm_simple_display_pipe *pipe,
				 struct drm_crtc_state *cstate)
{
	struct drm_crtc *crtc = &pipe->crtc;
	struct drm_device *drm = crtc->dev;
	struct gm12u320_drm_dev_private *priv = drm->dev_private;
	const struct drm_display_mode *mode = &cstate->mode;
	u32 ppl, hsw, hfp, hbp;
	u32 lpp, vsw, vfp, vbp;
	u32 cpl;
	int ret;

	ret = clk_set_rate(priv->clk, mode->clock * 1000);
	if (ret) {
		dev_err(drm->dev,
			"Failed to set pixel clock rate to %d: %d\n",
			mode->clock * 1000, ret);
	}

	clk_prepare_enable(priv->clk);

	ppl = (mode->hdisplay / 16) - 1;
	hsw = mode->hsync_end - mode->hsync_start - 1;
	hfp = mode->hsync_start - mode->hdisplay - 1;
	hbp = mode->htotal - mode->hsync_end - 1;

	lpp = mode->vdisplay - 1;
	vsw = mode->vsync_end - mode->vsync_start - 1;
	vfp = mode->vsync_start - mode->vdisplay;
	vbp = mode->vtotal - mode->vsync_end;

	cpl = mode->hdisplay - 1;

	printk("Stubbed section 1 in gm12u320_display_enable\n");

	spin_lock(&priv->tim2_lock);

        printk("Stubbed section 2 in gm12u320_display_enable\n");
	spin_unlock(&priv->tim2_lock);

        printk("Stubbed section 3 in gm12u320_display_enable\n");

	drm_panel_prepare(priv->connector.panel);

	printk("Stubbed section 4 in gm12u320_display_enable\n");

	drm_panel_enable(priv->connector.panel);

	drm_crtc_vblank_on(crtc);
}

void gm12u320_display_disable(struct drm_simple_display_pipe *pipe)
{
	struct drm_crtc *crtc = &pipe->crtc;
	struct drm_device *drm = crtc->dev;
	struct gm12u320_drm_dev_private *priv = drm->dev_private;

	drm_crtc_vblank_off(crtc);

	drm_panel_disable(priv->connector.panel);

	/* Disable and Power Down */
	printk("Stubbed section 1 in gm12u320_display_disable\n");

	drm_panel_unprepare(priv->connector.panel);

	clk_disable_unprepare(priv->clk);
}

static void gm12u320_display_update(struct drm_simple_display_pipe *pipe,
				 struct drm_plane_state *old_pstate)
{
	struct drm_crtc *crtc = &pipe->crtc;
	struct drm_pending_vblank_event *event = crtc->state->event;
	struct drm_plane *plane = &pipe->plane;
	struct drm_plane_state *pstate = plane->state;
	struct drm_framebuffer *fb = pstate->fb;
	unsigned long flags = 0;

	if (fb) {
		printk("Stubbed section 1 in gm12u320_display_update\n");
	}

	if (event) {
		crtc->state->event = NULL;

		spin_lock_irqsave(&crtc->dev->event_lock, flags);
		if (crtc->state->active && drm_crtc_vblank_get(crtc) == 0)
			drm_crtc_arm_vblank_event(crtc, event);
		else
			drm_crtc_send_vblank_event(crtc, event);
		spin_unlock_irqrestore(&crtc->dev->event_lock, flags);
	}
}

int gm12u320_enable_vblank(struct drm_device *drm, unsigned int crtc)
{
	printk("Stubbed section 1 in gm12u320_enable_vblank\n");

	return 0;
}

void gm12u320_disable_vblank(struct drm_device *drm, unsigned int crtc)
{
	printk("Stubbed section 1 in gm12u320_disable_vblank\n");
}

static int gm12u320_display_prepare_fb(struct drm_simple_display_pipe *pipe,
				    struct drm_plane_state *plane_state)
{
	return drm_fb_cma_prepare_fb(&pipe->plane, plane_state);
}

static const struct drm_simple_display_pipe_funcs gm12u320_display_funcs = {
	.check = gm12u320_display_check,
	.enable = gm12u320_display_enable,
	.disable = gm12u320_display_disable,
	.update = gm12u320_display_update,
	.prepare_fb = gm12u320_display_prepare_fb,
};

static int gm12u320_clk_div_choose_div(struct clk_hw *hw, unsigned long rate,
				    unsigned long *prate, bool set_parent)
{
	int best_div = 1;
	return best_div;
}

static long gm12u320_clk_div_round_rate(struct clk_hw *hw, unsigned long rate,
				     unsigned long *prate)
{
	int div = gm12u320_clk_div_choose_div(hw, rate, prate, true);

	return DIV_ROUND_UP_ULL(*prate, div);
}

static unsigned long gm12u320_clk_div_recalc_rate(struct clk_hw *hw,
					       unsigned long prate)
{
	return 0;
}

static int gm12u320_clk_div_set_rate(struct clk_hw *hw, unsigned long rate,
				  unsigned long prate)
{
	struct gm12u320_drm_dev_private *priv =
		container_of(hw, struct gm12u320_drm_dev_private, clk_div);

	spin_lock(&priv->tim2_lock);
	printk("Stubbed section 1 in gm12u320_clk_div_set_rate\n");
	spin_unlock(&priv->tim2_lock);

	return 0;
}

static const struct clk_ops gm12u320_clk_div_ops = {
	.recalc_rate = gm12u320_clk_div_recalc_rate,
	.round_rate = gm12u320_clk_div_round_rate,
	.set_rate = gm12u320_clk_div_set_rate,
};

int gm12u320_display_init(struct drm_device *drm)
{
	struct gm12u320_drm_dev_private *priv = drm->dev_private;
	int ret;
	static const u32 formats[] = {
		DRM_FORMAT_ARGB8888,
		DRM_FORMAT_RGB888,
	};

	ret = drm_simple_display_pipe_init(drm, &priv->pipe,
					   &gm12u320_display_funcs,
					   formats, ARRAY_SIZE(formats),
					   &priv->connector.connector);
	if (ret)
		return ret;

	return 0;
}
