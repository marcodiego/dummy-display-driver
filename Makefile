gm12u320_drm-y +=	gm12u320_connector.o \
		gm12u320_display.o \
		gm12u320_drv.o

gm12u320_drm-$(CONFIG_DEBUG_FS) += gm12u320_debugfs.o

obj-$(CONFIG_DRM_GM12U320) += gm12u320_drm.o
