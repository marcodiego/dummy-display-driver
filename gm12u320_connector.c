/*
 * Copyright (c) 2017 Marco Diego Aurélio Mesquita <marcodiegomesquita@gmail.com>
 *
 * Parts of this file were based on sources as follows:
 *
 * (C) COPYRIGHT 2012-2013 ARM Limited. All rights reserved.
 * Copyright (c) 2006-2008 Intel Corporation
 * Copyright (c) 2007 Dave Airlie <airlied@linux.ie>
 * Copyright (C) 2011 Texas Instruments
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms of
 * such GNU licence.
 *
 */

/**
 * gm12u320_drm_connector.c
 * Implementation of the connector functions for GM12U320 DRM
 */
#include <linux/version.h>
#include <linux/shmem_fs.h>
#include <linux/dma-buf.h>

#include <drm/drmP.h>
#include <drm/drm_atomic_helper.h>
#include <drm/drm_crtc_helper.h>
#include <drm/drm_of.h>
#include <drm/drm_panel.h>

#include "gm12u320_drm.h"

static void gm12u320_connector_destroy(struct drm_connector *connector)
{
	struct gm12u320_drm_connector *gm12u320_connector =
		to_gm12u320_connector(connector);

	if (gm12u320_connector->panel)
		drm_panel_detach(gm12u320_connector->panel);

	drm_connector_unregister(connector);
	drm_connector_cleanup(connector);
}

static enum drm_connector_status gm12u320_connector_detect(struct drm_connector
							*connector, bool force)
{
    if (drm_device_is_unplugged(connector->dev))
        return connector_status_disconnected;

    return connector_status_connected;
}

/*
 * Note this assumes this driver is only ever used with the Acer C120, if we
 * add support for other devices the vendor and model should be parameterized.
 */
static struct edid gm12u320_edid = {
	.header		= { 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00 },
	.mfg_id		= { 0x04, 0x72 },	/* "ACR" */
	.prod_code	= { 0x20, 0xc1 },	/* C120h */
	.mfg_week	= 1,
	.mfg_year	= 1,
	.version	= 1,			/* EDID 1.3 */
	.revision	= 3,			/* EDID 1.3 */
	.input		= 0x80,			/* Digital input */
	.features	= 0x02,			/* Pref timing in DTD 1 */
	.standard_timings = { { 1, 1 }, { 1, 1 }, { 1, 1 }, { 1, 1 },
			      { 1, 1 }, { 1, 1 }, { 1, 1 }, { 1, 1 } },
	.detailed_timings = { {
		.pixel_clock = 3383,
		/* hactive = 852, hblank = 256 */
		.data.pixel_data.hactive_lo = 0x54,
		.data.pixel_data.hblank_lo = 0x00,
		.data.pixel_data.hactive_hblank_hi = 0x31,
		/* vactive = 480, vblank = 28 */
		.data.pixel_data.vactive_lo = 0xe0,
		.data.pixel_data.vblank_lo = 0x1c,
		.data.pixel_data.vactive_vblank_hi = 0x10,
		/* hsync offset 40 pw 128, vsync offset 1 pw 4 */
		.data.pixel_data.hsync_offset_lo = 0x28,
		.data.pixel_data.hsync_pulse_width_lo = 0x80,
		.data.pixel_data.vsync_offset_pulse_width_lo = 0x14,
		.data.pixel_data.hsync_vsync_offset_pulse_width_hi = 0x00,
		/* Digital separate syncs, hsync+, vsync+ */
		.data.pixel_data.misc = 0x1e,
	}, {
		.pixel_clock = 0,
		.data.other_data.type = 0xfd, /* Monitor ranges */
		.data.other_data.data.range.min_vfreq = 59,
		.data.other_data.data.range.max_vfreq = 61,
		.data.other_data.data.range.min_hfreq_khz = 29,
		.data.other_data.data.range.max_hfreq_khz = 32,
		.data.other_data.data.range.pixel_clock_mhz = 4, /* 40 MHz */
		.data.other_data.data.range.flags = 0,
		.data.other_data.data.range.formula.cvt = {
			0xa0, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 },
	}, {
		.pixel_clock = 0,
		.data.other_data.type = 0xfc, /* Model string */
		.data.other_data.data.str.str = {
			'C', '1', '2', '0', 'P', 'r', 'o', 'j', 'e', 'c',
			't', 'o', 'r' },
	}, {
		.pixel_clock = 0,
		.data.other_data.type = 0xfe, /* Unspecified text / padding */
		.data.other_data.data.str.str = {
			'\n', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ',  ' ' },
	} },
	.checksum = 0x40,
};

static int gm12u320_connector_helper_get_modes(struct drm_connector *connector)
{
	drm_mode_connector_update_edid_property(connector, &gm12u320_edid);
	return drm_add_edid_modes(connector, &gm12u320_edid);
}

const struct drm_connector_funcs connector_funcs = {
	.fill_modes = drm_helper_probe_single_connector_modes,
	.destroy = gm12u320_connector_destroy,
	.detect = gm12u320_connector_detect,
	.dpms = drm_atomic_helper_connector_dpms,
	.reset = drm_atomic_helper_connector_reset,
	.atomic_duplicate_state = drm_atomic_helper_connector_duplicate_state,
	.atomic_destroy_state = drm_atomic_helper_connector_destroy_state,
};

const struct drm_connector_helper_funcs connector_helper_funcs = {
	.get_modes = gm12u320_connector_helper_get_modes,
};

int gm12u320_connector_init(struct drm_device *dev)
{
	struct gm12u320_drm_dev_private *priv = dev->dev_private;
	struct gm12u320_drm_connector *gm12u320_connector = &priv->connector;
	struct drm_connector *connector = &gm12u320_connector->connector;

	drm_connector_init(dev, connector, &connector_funcs,
			   DRM_MODE_CONNECTOR_Unknown);
	drm_connector_helper_add(connector, &connector_helper_funcs);

	return 0;
}

